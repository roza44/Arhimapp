﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Arhimapp.Model
{
    public class ArchWork
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string ConstructionDate { get; set; }
        public string ArchitectFirstName { get; set; }
        public string ArchitectLastName { get; set; }
        public string Epoch { get; set; }
        public string WorldWonder { get; set; }
        public string Protection { get; set; }
        public string Purpose { get; set; }
        public string Image { get; set; }

        public ArchWork()
        {

        }

        public ArchWork(ArchWork aw)
        {
            this.Id = aw.Id;
            this.Name = aw.Name;
            this.City = aw.City;
            this.ConstructionDate = aw.ConstructionDate;
            this.ArchitectFirstName = aw.ArchitectFirstName;
            this.ArchitectLastName = aw.ArchitectLastName;
            this.Epoch = aw.Epoch;
            this.WorldWonder = aw.WorldWonder;
            this.Protection = aw.Protection;
            this.Purpose = aw.Purpose;
        }

        public BitmapImage showImage()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                return new BitmapImage(new Uri(this.Image));
            }
            return null;
        }
    }
}
