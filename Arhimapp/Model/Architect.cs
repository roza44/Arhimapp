﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Arhimapp.Model
{
    public class Architect
    {
        public string Id {get; set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DeathDate { get; set; }
        public string Country { get; set; }
        public string Image { get; set; }

        public Architect() { }

        public Architect(Architect ar)
        {
            this.Id = ar.Id;
            this.FirstName = ar.FirstName;
            this.LastName = ar.LastName;
            this.DateOfBirth = ar.DateOfBirth;
            this.DeathDate = ar.DeathDate;
            this.Country = ar.Country;
            this.Image = ar.Image;
        }

        public BitmapImage showImage()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                return new BitmapImage(new Uri(Image));
            }
            return null;
        }
    }
    

}
