﻿using Arhimapp.Model;
using Arhimapp.Serialization;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Arhimapp.AddArhiForm
{
    /// <summary>
    /// Interaction logic for AddArhiForm.xaml
    /// </summary>
    public partial class AddArhiForm : Window
    {
        public Architect ArchitectToAdd { get; set; }
        public Architect realArchitect;
        public ImageSource archImage { get; set; }
        public bool IsUpdate = false;

        public AddArhiForm()
        {
            ArchitectToAdd = new Architect();
            this.ArchitectToAdd.DateOfBirth = null;
            this.ArchitectToAdd.DeathDate = null;

            this.DataContext = this.ArchitectToAdd;
            InitializeComponent();
            this.formButton.Content = "Add Architect";

        }
        public AddArhiForm(Architect architect, Architect original)
        {

            //this is update
            this.IsUpdate = true;
            realArchitect = original;
            this.ArchitectToAdd = architect;
            this.DataContext = architect;

            InitializeComponent();
            this.formButton.Content = "Update Architect";

        }

        private void loadImage(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                ArchitectToAdd.Image = openFileDialog.FileName;
                this.archImage = new BitmapImage(new Uri(openFileDialog.FileName));
                image.Source = this.archImage;
            }
        }
        private void saveArchitect(object sender, RoutedEventArgs e)
        {

            if (!this.validateInputFields(this.ArchitectToAdd))
            {
                MessageBox.Show("Fields are required!", "Warning");
                return;
                    
            }
            MainWindow main = (MainWindow)Application.Current.MainWindow;

            if (!IsUpdate)
            {
                main.AddArhitect(this.ArchitectToAdd);
            }
            else
            {
                realArchitect.FirstName = ArchitectToAdd.FirstName;
                realArchitect.LastName = ArchitectToAdd.LastName;
                realArchitect.DateOfBirth = ArchitectToAdd.DateOfBirth;
                realArchitect.DeathDate = ArchitectToAdd.DeathDate;
                main.UpdateArchitect(this.ArchitectToAdd);
            }
           
           
            this.Close();
        }
        private Boolean validateInputFields(Architect architect)
        {
            if (string.IsNullOrEmpty(architect.FirstName)) return false;
            if (string.IsNullOrEmpty(architect.LastName)) return false;
            if (string.IsNullOrEmpty(architect.Country)) return false;
            if (architect.DateOfBirth == null) return false;
            if (architect.DeathDate == null) return false;

            return true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //this.ArchitectToAdd = backup;
            this.Close();
        }
    }
}
