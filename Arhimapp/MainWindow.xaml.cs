﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Arhimapp.AddArhiForm;
using Arhimapp.Model;
using Arhimapp.Serialization;

namespace Arhimapp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public ObservableCollection<Architect> Architects { get; set; }
        public ObservableCollection<ArchWork> Buildings { get; set; }
        private ImageSource source;

        public MainWindow()
        {
            
            InitializeComponent();

            this.Architects = new ObservableCollection<Architect>();
            this.Buildings = new ObservableCollection<ArchWork>();
            this.DataContext = this;
        }

        private void CloseApplication(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void AddArhitect(object sender, RoutedEventArgs e)
        {
            AddArhiForm.AddArhiForm addArhitectForm = new AddArhiForm.AddArhiForm();
            addArhitectForm.Show();
        }

        private void AddArhitectBuilding(object sender, RoutedEventArgs e)
        {
            AddArchWorkForm.AddArchWorkForm addArchWork = new AddArchWorkForm.AddArchWorkForm();
            addArchWork.Show();
        }

        public void AddArhitect(Architect architect)
        {
            architect.Id = (this.Architects.Count() + 1).ToString();
            this.Architects.Add(architect);
            Serialization.SerializeArchitect.SerializeArchitects(Architects);
        }

        public Architect UpdateArchitect(Architect architect)
        {
            //foreach (Architect ar in Architects)
            //{
            //    if (ar.Id.Equals(architect.Id))
            //    {
            //        ar.FirstName = architect.FirstName;
            //        ar.LastName = architect.LastName;
            //        ar.DateOfBirth = architect.DateOfBirth;
            //        ar.DeathDate = architect.DeathDate;
            //    }
            //}
            Architects.Remove(Architects.Where(i => i.Id == architect.Id).Single());
            Architects.Add(architect);

            return null;
        }
        public void updateBuilding(ArchWork work)
        {
            Buildings.Remove(Buildings.Where(i => i.Id == work.Id).Single());
            Buildings.Add(work);

        }

        public void AddBuliding(ArchWork building)
        {
            building.Id = (this.Buildings.Count() + 1).ToString();
            this.Buildings.Add(building);
            AddArchWorkForm.AddArchWorkForm.SaveToXML(Buildings);
        }

        private void ArhitectTable(object sender, RoutedEventArgs e)
        {
            TableView.TableView table = new TableView.TableView(this.Architects);
            table.Show();
        }

        private void ArchitectWorkTable(object sender, RoutedEventArgs e)
        {
            TableView.TableViewWork table = new TableView.TableViewWork(this.Buildings);
            table.Show();
        }

        private void StackPanel_MouseMove(object sender, MouseEventArgs e)
        {
            StackPanel ellipse = sender as StackPanel;

            ImageSource source = ellipse
                                     .Children
                                     .OfType<Image>()
                                     .FirstOrDefault().Source;

            this.source = source;
            if (ellipse != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(ellipse,
                                     source,
                                     DragDropEffects.Copy);
            }
        }

        private void StackPanel_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {

        }

        private void StackPanel_DragEnter(object sender, DragEventArgs e)
        {
        }

        private void StackPanel_DragLeave(object sender, DragEventArgs e)
        {
           
        }

        private void StackPanel_DragOver(object sender, DragEventArgs e)
        {

        }

        private void StackPanel_Drop(object sender, DragEventArgs e)
        {
            Canvas canvas = sender as Canvas;
            Point position = Mouse.GetPosition(canvas);

            Image image = new Image();
            image.Source = this.source;

            Canvas.SetLeft(image, position.X);
            Canvas.SetTop(image, position.Y);

            image.Width = 20;
            image.Height = 20;

            Main.Children.Add(image);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string path = "Help\\Pocetak.htm";
            System.Diagnostics.Process.Start(path);
        }
    }
}
