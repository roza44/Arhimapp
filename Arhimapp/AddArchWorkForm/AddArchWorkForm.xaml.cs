﻿using Arhimapp.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Arhimapp.AddArchWorkForm
{
    /// <summary>
    /// Interaction logic for AddArchWorkForm.xaml
    /// </summary>
    public partial class AddArchWorkForm : Window
    {
        private ArchWork real;

        public ArchWork ArchWorkToAdd { get; set; }
        public ImageSource archImage { get; set; }
        public bool isUpdated = false; 

        public AddArchWorkForm()
        {
            
            this.ArchWorkToAdd = new ArchWork();
            this.DataContext = ArchWorkToAdd;
            InitializeComponent();
            formButton.Content = "Add Architect work";
        }

        public AddArchWorkForm(ArchWork deepAW, ArchWork original)
        {
            
            isUpdated = true;
            this.ArchWorkToAdd = deepAW;
            this.real = original;
            this.DataContext = deepAW;
            InitializeComponent();
            formButton.Content = "Change Architect work";

        }

        public void saveArchitectWork(object sender, RoutedEventArgs e)
        {
            bool inputsFilled = allFieldsFilled();
            
            if (!inputsFilled)
            {
                MessageBox.Show("You have to fill all inputs.");
                return;
            }
            MainWindow main = (MainWindow)Application.Current.MainWindow;

            if (!isUpdated)
            {
                main.AddBuliding(this.ArchWorkToAdd);

            }
            else
            {
                main.updateBuilding(this.ArchWorkToAdd);

            }

            this.Close();

        }

        public static void SaveToXML(ObservableCollection<Model.ArchWork> archWorks)
        {
            XmlSerializer serialiser = new XmlSerializer(typeof(List<ArchWork>));

            // Create the TextWriter for the serialiser to use
            TextWriter filestream = new StreamWriter("../../output.xml");

            //write to the file
            serialiser.Serialize(filestream, archWorks);

            // Close the file
            filestream.Close();
        }

        public Boolean allFieldsFilled()
        {
            if (
               this.archWorkName.Text != "" &&
               this.protection.Text != "" &&
               this.purpose.Text != "" &&
               this.worldWonder.Text != "" &&
               this.epoch.Text != "" &&
               this.constYear.Text != "" &&
               this.architectFirstName.Text != "" &&
               this.architectSurname.Text != "" &&
               this.city.Text != ""
                )
            {
                return true;
            }

            return false;
        }

        public void loadImage(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                ArchWorkToAdd.Image = openFileDialog.FileName;
                archImage = new BitmapImage(new Uri(openFileDialog.FileName));
                image.Source = this.archImage;
            }
        }
    }
}
