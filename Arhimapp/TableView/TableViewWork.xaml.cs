﻿using Arhimapp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Arhimapp.TableView
{
    /// <summary>
    /// Interaction logic for TableViewWork.xaml
    /// </summary>
    public partial class TableViewWork : Window
    {
        public ObservableCollection<Model.ArchWork> Buildings { get; set; }
        public ICollectionView cv { get; set; }
        public List<ArchWork> archWorks = new List<ArchWork>();
        public AddArchWorkForm.AddArchWorkForm awf;

        public TableViewWork(ObservableCollection<Model.ArchWork> buildings)
        {
            InitializeComponent();
            this.Buildings = buildings;

            this.DataContext = this;

            this.cv = CollectionViewSource.GetDefaultView(Buildings);
        }

        public void filterList(object sender, EventArgs e)
        {
            if (searchText.Text.Length == 0)
                cv.Filter = null;
            else
            {
                cv.Filter = o =>
                {
                    ArchWork sci = o as ArchWork;
                    string[] words = searchText.Text.Split(' ').Where(word => word.Length > 0).ToArray();
                    return words.Any(word =>
                           sci.Id.ToLower().Contains(word.ToLower())
                        || sci.Name.ToLower().Contains(word.ToLower())
                        || sci.City.ToLower().Contains(word.ToLower())
                        || sci.ArchitectFirstName.ToLower().Contains(word.ToLower())
                        || sci.ArchitectLastName.ToLower().Contains(word.ToLower())
                        || sci.Epoch.ToLower().Contains(word.ToLower())
                        || sci.WorldWonder.ToLower().Contains(word.ToLower())
                        || sci.Protection.ToLower().Contains(word.ToLower())
                        || sci.Purpose.ToLower().Contains(word.ToLower()));
                };
            }
        }

        public void Change(object sender, EventArgs e)
        {
            ArchWork aw = (ArchWork)this.WorkTable.SelectedValue;

            if (aw == null) return;

            ArchWork deepAW = new ArchWork(aw);

            awf = new AddArchWorkForm.AddArchWorkForm(deepAW, aw);
            awf.Show();
        }
    }
}
