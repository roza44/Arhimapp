﻿using Arhimapp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Arhimapp.TableView
{
    /// <summary>
    /// Interaction logic for TableView.xaml
    /// </summary>
    public partial class TableView : Window
    {
        public ObservableCollection<Model.Architect> Architects { get; set; }
        public ICollectionView cv { get; set; }
        public AddArhiForm.AddArhiForm architectForm;

        public TableView(ObservableCollection<Model.Architect> architects)
        {
            InitializeComponent();
            this.Architects = architects;

            this.DataContext = this;

            this.cv = CollectionViewSource.GetDefaultView(Architects);
        }

        private void filterList(object sender, RoutedEventArgs e)
        {
            if (searchText.Text.Length == 0)
                cv.Filter = null;
            else
            {
                cv.Filter = o =>
                {
                    Architect sci = o as Architect;
                    string[] words = searchText.Text.Split(' ').Where(word => word.Length > 0).ToArray();
                    return words.Any(word =>
                           sci.Id.ToLower().Contains(word.ToLower())
                        || sci.FirstName.ToLower().Contains(word.ToLower())
                        || sci.LastName.ToLower().Contains(word.ToLower())
                        || sci.Country.ToLower().Contains(word.ToLower())
                        || sci.DateOfBirth.ToString().ToLower().Contains(word.ToLower())
                        || sci.DeathDate.ToString().ToLower().Contains(word.ToLower())
                        );
                };
            }
        }

        private void UpdateArchitect(object sender, RoutedEventArgs e)
        {
            Architect architect = (Architect)this.ArchitectTable.SelectedValue;
            if (architect == null)
                return;
            Architect ar = new Architect(architect);

            architectForm = new AddArhiForm.AddArhiForm(ar, architect);
            architectForm.Show();
        }
    }
}
