﻿using Arhimapp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Arhimapp.Serialization
{
    public class SerializeArchitect
    {

        public static void SerializeArchitects(ObservableCollection<Architect> obbArchitects)
        {
            List<Architect> architects = obbArchitects.ToList();
  
            XmlSerializer x = new XmlSerializer(typeof(List<Architect>));
            TextWriter writer = new StreamWriter("../../architects.xml");
            x.Serialize(writer, architects);
        }
        public static List<Architect> DeserializeArchitects()
        {
            List<Architect> architects;

            XmlSerializer serializer = new XmlSerializer(typeof(List<Architect>));
            FileStream fs = new FileStream("../../architects.xml", FileMode.Open);
            architects = (List<Architect>)serializer.Deserialize(fs);
           
            return architects;
        }
    }
}
